class CardDeck {
  constructor() {
    const suits = ['D', 'H', 'S', 'C'];
    const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    this.deck = [];

    for (let i = 0; i < ranks.length; i++) {
      for (let j = 0; j < suits.length; j++) {
        this.deck.push({
          suit: suits[j],
          rank: ranks[i]
        });
      }
    }
  }

  getCard (howMany = 1) {
    let cards = [];

    for (let i = 0; i < howMany; i++) {
      const cardIndex = Math.floor(Math.random() * this.deck.length);
      cards.push(this.deck.splice(cardIndex, 1)[0]);
    }

    return cards;
  }
}

export default CardDeck;