class PokerHand {
  constructor (cardsArray) {
    let ranks = [];
    let suits = [];
    for (let i = 0; i < cardsArray.length; i++) {
      ranks.push(cardsArray[i].rank);
      suits.push(cardsArray[i].suit);
    }

    this.cards = {
      ranks,
      suits,
      array: cardsArray
    };

    this.ranksWeight = {
      '2': 2,
      '3': 3,
      '4': 4,
      '5': 5,
      '6': 6,
      '7': 7,
      '8': 8,
      '9': 9,
      '10': 10,
      'J': 11,
      'Q': 12,
      'K': 13,
      'A': 14
    };
  }

  getOutcome() {
    const sortRanks = (ranksArray, AtoB = false) => {
      const ranks = [...ranksArray];

      if (AtoB) {
        ranks.sort((b, a) => {
          if (this.ranksWeight[a] < this.ranksWeight[b]) return 1;
          if (this.ranksWeight[a] > this.ranksWeight[b]) return -1;
          return 0;
        });
      } else {
        ranks.sort((a, b) => {
          if (this.ranksWeight[a] < this.ranksWeight[b]) return 1;
          if (this.ranksWeight[a] > this.ranksWeight[b]) return -1;
          return 0;
        });
      }

      return ranks;
    };

    const elementsEqual = (array, count = array.length) => {
      for (let i = 0; i < count; i++) {
        if (array[i] !== array[0]) {
          return false;
        }
      }
      return true;
    };

    const isStraight = (ranksArray) => {
      let ranks = [...ranksArray];
      if (ranks.includes('A')) {
        const AIndex = ranks.indexOf('A');
        if (AIndex === 0 || AIndex === 4) {
          ranks.splice(AIndex, 1);
        }
      }

      for (let i = 0; i < ranks.length; i++) {
        if ((this.ranksWeight[ranks[0]] - this.ranksWeight[ranks[i]]) !== i) {
          return false;
        }
      }
      return true;
    };

    const countOfRanks = (ranksArray, returnEqualRanks = false) => {
      let ranks = {};
      for (let i = 0; i < ranksArray.length; i++) {
        if (!ranks[ranksArray[i]]) {
          ranks[ranksArray[i]] = 1;
        } else ranks[ranksArray[i]]++;
      }

      if (returnEqualRanks) {
        let counter = 0;

        const keys = Object.keys(ranks);
        for (let i = 0; i < keys.length; i++) {
          if (ranks[keys[i]] > 1) {
            counter++;
          }
        }

        return counter;
      } else {
        return Object.keys(ranks).length;
      }
    };

    switch (true) {
      case (this.cards.ranks.join('') === 'AKQJ10' && elementsEqual(this.cards.suits)):
        return 'Royal flush';
      case (isStraight(this.cards.ranks) && elementsEqual(this.cards.suits)):
        return 'Straight flush';
      case (elementsEqual(this.cards.ranks, 4) || elementsEqual(sortRanks(this.cards.ranks, true), 4)):
        return 'Four of a kind';
      case (countOfRanks(this.cards.ranks) === 2):
        return 'Full house';
      case (elementsEqual(this.cards.suits)):
        return 'Flush';
      case (isStraight(this.cards.ranks)):
        return 'Straight';
      case (countOfRanks(this.cards.ranks, true) === 1 && countOfRanks(this.cards.ranks) === 3):
        return 'Three of a kind';
      case (countOfRanks(this.cards.ranks, true) === 2 && countOfRanks(this.cards.ranks) === 3):
        return 'Two pairs';
      case (countOfRanks(this.cards.ranks) === 4):
        return 'One pair';
      default:
        return `High card (${sortRanks(this.cards.ranks)[0]})`;
    }
  }
}

export default PokerHand;