import React, { Component } from 'react';
import './App.css';
import Card from './components/Card/Card';
import CardDeck from './game/CardDeck';
import PokerHand from './game/PokerHand';

class App extends Component {
  initCards = new CardDeck().getCard(5);

  state = {
    cards: this.initCards,
    currentHand: new PokerHand(this.initCards).getOutcome()
  };

  shuffleCards = () => {
    const newCards = new CardDeck().getCard(5);
    this.setState({
      cards: newCards,
      currentHand: new PokerHand(newCards).getOutcome()
    });
  };

  printCards = () => {
    const cardsInfoArray = this.state.cards;

    const cardsArray = [];
    for (let i = 0; i < cardsInfoArray.length; i++) {
      cardsArray.push(<Card
        suit={cardsInfoArray[i].suit}
        rank={cardsInfoArray[i].rank}
        key={i}
      />);
    }

    return cardsArray;
  };

  render() {
    return (
      <div className="container">
        <div>{this.state.currentHand}</div>
        <div className="cardsBlock">
          {this.printCards()}
        </div>
        <button onClick={this.shuffleCards} className="shuffleBtn">Shuffle Cards</button>
      </div>
    );
  }
}

export default App;
