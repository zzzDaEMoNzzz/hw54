import React from 'react';
import './Card.css';

const Card = props => {
  const suits = {
    'D': "♦",
    'H': '♥',
    'S': '♠',
    'C': '♣'
  };

  const suitsName = {
    'D': "diams",
    'H': 'hearts',
    'S': 'spades',
    'C': 'clubs'
  };

  const cardClassName = `Card Card-rank-${props.rank.toLowerCase()} Card-${suitsName[props.suit]}`;

  return (
    <div className={cardClassName}>
      <span className="Card-rank">{props.rank}</span>
      <span className="Card-suit">{suits[props.suit]}</span>
    </div>
  );
};

export default Card;